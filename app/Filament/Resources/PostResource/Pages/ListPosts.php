<?php

namespace App\Filament\Resources\PostResource\Pages;

use App\Filament\Resources\PostResource;
use App\Imports\PostsImport;
use Filament\Forms\Components\FileUpload;
use Filament\Pages\Actions;
use Filament\Pages\Actions\Action;
use Filament\Resources\Pages\ListRecords;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ListPosts extends ListRecords
{
    protected static string $resource = PostResource::class;

    protected function getActions(): array
    {
        return [
            Actions\CreateAction::make(),
            Action::make('upload')
                ->action(function(array $data){
                    Excel::import(new PostsImport, $data['file'], 'public');

                    $this->notify('success', 'Imported successfully!');
                })
            ->form([
                FileUpload::make('file')
                    ->preserveFilenames()
                    ->required()
                   ])
            ,
        ];
    }
}
