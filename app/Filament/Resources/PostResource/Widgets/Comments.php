<?php

namespace App\Filament\Resources\PostResource\Widgets;

use App\Models\Comment;
use Filament\Widgets\Widget;
use Illuminate\Database\Eloquent\Model;

class Comments extends Widget
{
    public ?Model $record = null;

    protected function getViewData(): array
    {
        return [
            'comments' => Comment::where('post_id', $this->record->id)->get()
        ];
    }



    protected static string $view = 'filament.resources.post-resource.widgets.comments';
}
