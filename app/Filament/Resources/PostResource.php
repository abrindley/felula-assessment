<?php

namespace App\Filament\Resources;

use App\Filament\Resources\PostResource\Pages;
use App\Filament\Resources\PostResource\RelationManagers;
use App\Models\Post;
use Filament\Forms;
use Filament\Forms\Components\Grid;
use Filament\Forms\Components\RichEditor;
use Filament\Forms\Components\Select;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Filament\Tables\Filters\MultiSelectFilter;
use Filament\Widgets\Widget;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class PostResource extends Resource
{
    protected static ?string $model = Post::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                 Grid::make(4)
                     ->schema([
                      Forms\Components\TextInput::make('title')
                          ->columnSpan(2)
                          ->required()
                          ->maxLength(255),
                      Select::make('author_id')
                          ->columnSpan(2)
                          ->relationship('author', 'name')
                          ->default(auth()->id()),
                      RichEditor::make('content')
                          ->columnSpan(4)
                          ->toolbarButtons([
                               'blockquote',
                               'bold',
                               'bulletList',
                               'codeBlock',
                               'h2',
                               'h3',
                               'italic',
                               'link',
                               'orderedList',
                               'redo',
                               'strike',
                               'undo',
                           ])
                          ->required()
                          ->maxLength(65535),
                  ])
             ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
              Tables\Columns\TextColumn::make('title')->wrap(),
              Tables\Columns\TextColumn::make('author.name'),
              Tables\Columns\TextColumn::make('created_at')
                  ->dateTime()->since(),
              Tables\Columns\TextColumn::make('updated_at')
                  ->dateTime()->since(),
            ])
            ->filters([
              MultiSelectFilter::make('author')->relationship('author', 'name')
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListPosts::route('/'),
            'create' => Pages\CreatePost::route('/create'),
            'view' => Pages\ViewPost::route('/{record}'),
            'edit' => Pages\EditPost::route('/{record}/edit'),
        ];
    }

    public static function getWidgets(): array
    {
        return [
            PostResource\Widgets\Comments::class
        ];
    }


}
