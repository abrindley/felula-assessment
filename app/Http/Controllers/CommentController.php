<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use App\Models\Post;
use Illuminate\Http\Request;


class CommentController extends Controller
{
    public function store(CommentRequest $request)
    {
        $data = $request->validated();


        $comment = Comment::create([
            'content' => $data['content'],
            'post_id' => $data['post_id']
        ]);

        $post = Post::find($data['post_id']);

        #todo: understand why this isn't working as it should be
        //$comment = $post->comments()->create(['content' => $data['content']]);

        if(isset($comment->id))
        {
            return redirect()->route('post.show', $post)->with('status', 'Comment added successfully');
        }
    }
}

