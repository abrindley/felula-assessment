<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Http\Resources\PostResource;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends BaseController
{
    public function index()
    {
        $posts = Post::all();
        return $this->handleResponse(PostResource::collection($posts), 'Posts have been retrieved!');
    }

    public function store(PostRequest $request)
    {
        #TODO: review what happens if this fails
        $data = $request->validated();

        $post = Post::create($data);
        return $this->handleResponse(new PostResource($post), 'Post created!', 201);
    }

    public function show(Post $post)
    {
        ##TODO: review what happens if this fails to find the post
        return $this->handleResponse(new PostResource($post), 'Post retrieved.');
    }

    public function update(PostRequest $request, Post $post)
    {
        $data = $request->validated();

        $post->update($data);

        return $this->handleResponse(new PostResource($post), 'Post updated successfully!');
    }

    public function destroy(Post $post)
    {
        $post->delete();
        return $this->handleResponse([], 'Post deleted!', 204);
    }
}
