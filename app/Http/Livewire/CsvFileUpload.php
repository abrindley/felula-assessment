<?php

namespace App\Http\Livewire;

use Filament\Forms\Components\FileUpload;
use Filament\Forms\Concerns\InteractsWithForms;
use Filament\Forms\Contracts\HasForms;
use Livewire\Component;
use Livewire\TemporaryUploadedFile;

class CsvFileUpload extends Component implements HasForms
{
    use InteractsWithForms;

    public $file = null;

    protected $listeners = ['dump-form-data' => 'upload'];

    protected function getFormSchema(): array
    {
        return [
            FileUpload::make('file')
        ];

    }

    public function upload($data)
    {
        dd($data);
    }

    public function render()
    {
        return view('livewire.csv-file-upload');
    }

    public function dispatchFormEvent(...$args): void
    {
        // TODO: Implement dispatchFormEvent() method.
    }



    public function getComponentFileAttachment(string $statePath): ?TemporaryUploadedFile
    {
        // TODO: Implement getComponentFileAttachment() method.
    }

    public function getComponentFileAttachmentUrl(string $statePath): ?string
    {
        // TODO: Implement getComponentFileAttachmentUrl() method.
    }





    public function getUploadedFileUrls(string $statePath): ?array
    {
        return [];
    }



}
