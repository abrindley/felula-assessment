<?php

namespace App\Console\Commands;

use App\Models\Post;
use Illuminate\Console\Command;
use Vedmant\FeedReader\Facades\FeedReader;

class ImportFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $f = FeedReader::read('http://rss.cnn.com/rss/edition_world.rss');

        for($c = 0; $c < 10; $c++)
        {
            Post::create([
                 'title' => $f->get_items()[$c]->get_title(),
                 'content' => $f->get_items()[$c]->get_content(),
                 'author_id' => 1
             ]);
        }

    }
}
