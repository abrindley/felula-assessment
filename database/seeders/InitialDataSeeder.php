<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class InitialDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(User::where('email', 'anthony@anthonybrindley.com')->get()->isEmpty())
        {
            User::factory()->create([
                'name' => 'Anthony',
                'email' => 'anthony@anthonybrindley.com',
                'password' => Hash::make('secret123')
            ]);
        }

        $this->call(PostsSeeder::class);
    }
}
