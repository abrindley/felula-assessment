<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CommentTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
    }

    /** @test */
    function comments_belong_to_a_post()
    {
        $comment = (Comment::all()->count() > 0) ? Comment::all()->random() : Comment::factory()->create();

        $this->assertInstanceOf(BelongsTo::class, $comment->post());
        $this->assertInstanceOf(Post::class, $comment->post);
    }

    /** @test */
    function comments_are_rendered_on_post_view_pages_for_admins()
    {
        $auth = (User::all()->count() > 1) ? User::all()->random() : User::factory()->create();
        $post = (Post::where('author_id', $auth->id)->get()->count() > 1) ? Post::where('author_id', $auth->id)->get()->random() : Post::factory()
            ->create(['author_id' => $auth->id]);
        $comment = Comment::factory()->create(['post_id' => $post->id]);

        $this->actingAs($auth);

        $response = $this->get('/admin/posts/'.$post->id);

        $response->assertSee($comment->content);
    }

    /** @test */
    function comments_for_one_post_are_not_seen_on_the_view_pages_of_other_posts()
    {
        $auth = (User::all()->count() > 1) ? User::all()->random() : User::factory()->create();
        $post = (Post::where('author_id', $auth->id)->get()->count() > 1) ? Post::where('author_id', $auth->id)->get()->random() : Post::factory()
            ->create(['author_id' => $auth->id]);
        $comment = Comment::factory()->create(['post_id' => $post->id]);
        $newPost = Post::factory()->create(['author_id' => $auth->id]);

        $this->actingAs($auth);

        $response = $this->get('/admin/posts/'.$newPost->id);

        $response->assertDontSee($comment->content);
    }
}
