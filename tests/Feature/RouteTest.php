<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RouteTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
    }

    /**
     * @test
     * @dataProvider pages_with_responses
     */
    function pages_render_as_expected($url, $expectedResponseCode, $loggedIn = false)
    {
        if($loggedIn)
        {
            $auth = User::where('email', 'anthony@anthonybrindley.com')->first();
            if($auth) $this->actingAs($auth);
        }

        $response = $this->get($url);

        $response->assertStatus($expectedResponseCode);


    }

    public function pages_with_responses()
    {
        return [
            'Guest users can see login page' => ['/admin/login', 200],
            'logged in users can see dashboard' => ['/admin', 200, true],
            'Guest users cannot see dashboard' => ['/admin', 302],
        ];
    }

    /** @test */
    function logged_in_user_can_access_dashboard()
    {
        $this->withoutExceptionHandling();
        $auth = User::where('email', 'anthony@anthonybrindley.com')->first();

        $this->actingAs($auth);

        $response = $this->get('admin');

        $this->assertAuthenticated();
        $response->assertOk();


    }


}
