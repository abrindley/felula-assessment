<?php

namespace Tests\Feature;

use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
    }

    /** @test */
    function a_post_belongs_to_an_author()
    {

        $post = (Post::all()->count() > 0) ? Post::all()->random() : Post::factory()->create();

        $this->assertInstanceOf(BelongsTo::class, $post->author());
        $this->assertInstanceOf(User::class, $post->author);
    }


    /** @test */
    function a_post_can_have_many_comments()
    {
        $post = (Post::all()->count() > 0) ? Post::all()->random() : Post::factory()->create();

        if($post->comments->count() === 0){
            Comment::factory()->create(['post_id'=> $post->id]);
            $post = $post->refresh();
        }

        $this->assertInstanceOf(HasMany::class, $post->comments());
        $this->assertInstanceOf(Comment::class, $post->comments->first());

    }
}
