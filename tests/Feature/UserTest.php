<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
    }

    /** @test */
    function a_user_can_have_many_posts()
    {
        $user = (User::all()->count() > 0 ) ? User::all()->random() : User::factory()->create();

        $this->assertInstanceOf(HasMany::class, $user->posts());
        $this->assertInstanceOf(Post::class, $user->posts->first());
    }
}
