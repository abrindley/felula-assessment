<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostApiTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
    }

    private function authorizeUser()
    {
        $user = User::factory()->create();
        $resp = $user->createToken('testToken');
        $token = $resp->plainTextToken;

        $headers = ['Authorization' => "Bearer $token"];

        return [
            'headers' => $headers,
            'user' => $user->toArray()
        ];
    }

    ##TODO: authentication tests

    /** @test */
    function get_posts_via_api_returns_data_in_valid_format()
    {

        $headers = $this->authorizeUser();

        $response = $this->json('get', 'api/v1/posts', [], $headers['headers']);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'author',
                        'author_id',
                        'title',
                        'content',
                        'updated_at'
                    ]
                ]
      ]);
    }

    /** @test */
    function post_post_via_api_creates_post()
    {
        $headers = $this->authorizeUser();


        $payload = [
            'title' => 'This is a new title',
            'content' => 'Clearly this is some content',
            'author_id' => $headers['user']['id']
        ];

        $response = $this->json('post', 'api/v1/posts', $payload, $headers['headers']);

        $response
            ->assertStatus(201)
            ->assertJsonStructure([
                  'data' => [
                      'id',
                      'author',
                      'author_id',
                      'title',
                      'content',
                      'updated_at'
                  ]
              ]);

        $this->assertDatabaseHas('posts', $payload);
    }

    /** @test */
    function get_single_post_returns_data_in_correct_format()
    {
        $headers = $this->authorizeUser();

        $post = (Post::all()->count() > 0) ? Post::all()->random() : Post::factory()->create();

        $response = $this->json('get', 'api/v1/posts/'.$post->id, [], $headers['headers']);

        $response
            ->assertStatus(200)
            ->assertJsonStructure([
                  'data' => [
                      'id',
                      'author',
                      'author_id',
                      'title',
                      'content',
                      'updated_at'
                  ]
              ]);

    }

    /** @test */
    function update_via_api_works_as_expected()
    {
        $headers = $this->authorizeUser();

        $post = (Post::all()->count() > 0) ? Post::all()->random() : Post::factory()->create();

        $payload = [
            'title' => 'New Unique title',
            'content' => 'Some new content',
            'author_id' => $post->author_id
        ];

        $response = $this->json('patch', 'api/v1/posts/'.$post->id, $payload, $headers['headers']);

        $response
            ->assertOk();
        ##TODO: look into amending this test
//            ->assertExactJson([
//                'data' => [
//                    'author' => $post->author,
//                    'author_id' => $post->author_id,
//                    'content' => $payload['content'],
//                    'id' => $post->id,
//                    'title' => $payload['title']
//                ]
    //    ]);
    }

    /** @test */
    function delete_via_api_works_as_expected()
    {
        $headers = $this->authorizeUser();

        $post = (Post::all()->count() > 0) ? Post::all()->random() : Post::factory()->create();

        $response = $this->json('delete', 'api/v1/posts/'.$post->id, [], $headers['headers']);

        $response
            ->assertNoContent();
        $this->assertDatabaseMissing('posts', $post->toArray());
    }


}
