<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class SchemaTest extends TestCase
{
    public function setUp() :void
    {
        parent::setUp();
    }


    /**
     * @test
     * @dataProvider table_column_count_array
     */
    public function database_tables_have_expected_number_of_columns($table, $expectedValue)
    {
        $numberOfColumns = count(Schema::getColumnListing($table));
        $this->assertEquals($expectedValue, $numberOfColumns);
    }

    public function table_column_count_array()
    {
        return [
            'comments table has correct number of columns' => ['comments', 6],
            'posts table has correct number of columns' => ['posts', 6],
            'users table has correct number of columns' => ['users', 8]
        ];
    }

    /**
     * @test
     * @dataProvider databaseSchema
     */
    public function database_tables_have_expected_columns($table, $column)
    {
        $this->assertTrue(Schema::hasColumn($table, $column));
    }

    public function databaseSchema()
    {
        return [
            // comments
            'comments table has id column' => ['comments', 'id'],
            'comments table has content column' => ['comments', 'content'],
            'comments table has author_id column' => ['comments', 'author_id'],
            'comments table has post_id column' => ['comments', 'post_id'],
            'comments table has created_at column' => ['comments', 'created_at'],
            'comments table has updated_at column' => ['comments', 'updated_at'],

            // posts
            'posts table has id column' => ['posts', 'id'],
            'posts table has title column' => ['posts', 'title'],
            'posts table has content column' => ['posts', 'content'],
            'posts table has author_id column' => ['posts', 'author_id'],
            'posts table has created_at column' => ['posts', 'created_at'],
            'posts table has updated_at column' => ['posts', 'updated_at'],

            // users
            'users table has id column' => ['users', 'id'],
            'users table has name column' => ['users', 'name'],
            'users table has email column' => ['users', 'email'],
            'users table has email_verified_at column' => ['users', 'email_verified_at'],
            'users table has password column' => ['users', 'password'],
            'users table has remember_token column' => ['users', 'remember_token'],
            'users table has created_at column' => ['users', 'created_at'],
            'users table has updated_at column' => ['users', 'updated_at'],
        ];
    }
}
