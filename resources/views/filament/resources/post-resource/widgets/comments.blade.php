<x-filament::widget>
    <x-filament::card>
        <style>
            .comment{
                display: grid;
                grid-template-columns: 2fr 1fr;
            }
            .comment {

            }
        </style>
        <div class="comment-wrapper">
            @foreach($comments as $comment)
                <div class="comment mb-4 border-b border-gray-200 pb-4">
                    <div class="prose block w-full max-w-none p-3 opacity-70 shadow-sm">{{ $comment->content }}</div>
                    <div class="prose block w-full max-w-none p-3 opacity-70 shadow-sm">{{ $comment->created_at->diffForHumans() }}</div>
                </div>

            @endforeach
        </div>
    </x-filament::card>
</x-filament::widget>
