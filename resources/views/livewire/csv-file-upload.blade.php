<div>
    <form wire:submit.prevent="upload">
        {{ $this->form }}

        <button class="flex items-center justify-center w-full h-8 px-3" type="submit">Upload</button>
    </form>
</div>
