<div class="flex items-start space-x-4">

    <div class="min-w-0 flex-1">
        <form action="{{ route('comment.create') }}" method="POST" class="relative">
            @csrf
            <input type="hidden" name="post_id" value="{{ $post->id }}" />
            <div class="border border-gray-300 rounded-lg shadow-sm overflow-hidden focus-within:border-indigo-500 focus-within:ring-1 focus-within:ring-indigo-500">
                <label for="comment" class="ml-3 sr-only">Add your comment</label>
                <textarea rows="5" name="content" id="content" class="block w-full pl-3 py-3 border-0 resize-none focus:ring-0 sm:text-sm" placeholder="Add your comment..."></textarea>

                <div class="flex-shrink-0 p-3">
                    <button type="submit" class="inline-flex items-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">Post</button>
                </div>
            </div>

            {{-- Typically we would add some kind of user verification to this form - it feels naked currently. BUT it's not outlined as a requirement - just wanted to note that I am uncomfortable with this implementation as-is --}}

        </form>
    </div>
</div>