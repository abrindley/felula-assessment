# Felula Assessment project

This project has been developed to meet the criteria outlined in your technical assessment document. It includes
the following features: 

- Authentication system
- User Management
- Posts Management including a WYSIWYG editor for creating a post
- A basic fronted that includes a post archive as well as a post details screen
- The post details screen displays the full content of the post as well as allowing the user (if they are a guest) to post comments. Previous comments are shown in the list too. The form has basic validation as you'd expect.
- The application is able to import posts from a csv file
- The application is also setup to import posts from an RSS feed daily. Currently the schedule and the target are fixed, but it would be trivial to allow the user to define these as options to the command
- The application also supports Post management via API using default Laravel Sanctum authentication. Details below
- Some initial tests that cover the core functionality that has been developed. I ran out of time to add dusk tests, but typically I would.

## Installation

To install the application, clone the repository locally and follow the standard Laravel installation steps. For convenience they are: 

1. Create an env file. For Linux users: cp .env.example .env
2. Create a database and update the new .env file with those credentials. For reference, the application has been designed to run on a MySQL database instance
3. Run ```composer install```
4. Run ```npm install```
5. Navigate to the root directory for the application and run ```php artisan key:generate``` to create an application key
6. Migrate and seed the database by running  ```php artisan migrate --seed``` to add some seed data

## System backend

This application uses Laravel Filament to provide a lightweight authentication system. To access it, head to /admin/login and login with:
- email: anthony@anthonybrindley.com
- password: secret123

## CSV upload

You can upload a .csv file of posts by loggin in, navigating to the Posts resource (via 'Posts' link in the main navigation). You will see an 'upload' button
in the top right of the page. Upload a .csv file that meets the following requirements: 

- Has a header row that simply has 2 columns = 'title', 'content'
- Each subsequent row will be a post to be imported

You can quite easily amend the import script to accomodate different authors should you wish. Simply add a new 'author_id' column to the csv header row and update the class at ```app/Imports/PostsImport``` to accept the new column. Ensure there is a user with each submitted ID.   

## API

The system provides the following endpoints: 

### Authentication
- `````/api/v1/register````` (POST) Requires you to provide the following:
  - name (string|required)
  - email (email|required)
  - password (string|required)
  - confirm_password (string|required|{must match password})
- `````/api/v1/login````` (POST) Requires you to provide the following:
  - email (email|required)
  - password (string|required)

Both of these endpoints provide the user with a bearer token that allows them to interact with the API endpoints.

## Post CRUD - all routes authenticated and require a bearer token
- ```/api/v1/posts``` (GET) Will return a list of posts with relevant data for display
- ```/api/v1/posts``` (POST) Will create a new post. This endpoint requires the following:
  - title (string|required)
  - content (string|required)
- ```/api/v1/posts/{post_id}``` (GET) will return the full details for a particular post if it exists.
  - post_id (int|required)
- ``` /api/v1/posts/``` (PATCH) will allow the user to update a particular post if it exists. This endpoint requires the following:
  - title (optional|string) - provide if you want to update the title
  - content (optional|string) - provide if you want to update the content
- ```/api/v1/posts/{post_id}``` (DELETE) will delete the post resource if it exists
  - post_id(int_required)

## TESTS

You can run the tests included with this application via the command line. Simple type ```php artisan test``` to run the full suite. You may need to navigate to your phpunit installation directly and run ```./vendor/bin/phpunit``` 

## Support

Most queries can be solved using the fantastic documentation provided by the laravel team [here](https://laravel.com/docs/9.x/).
This application is using Laravel Filament for user management, so you can check the documentation [here](https://filamentphp.com/docs/2.x/admin/installation) for how to create a user from the command line etc.